require 'serialport' 
class PortHandler

  attr_reader :direction, :position, :time

  def initialize() # constructor
    secret = Rails.application.secrets
    @sp = SerialPort.new(secret.usb_port, #port
                         secret.baud_rate, #baud_rate
                         secret.data_bits, #data_bits
                         secret.stop_bits, #stop_bits
                         SerialPort::NONE #parity
                        )
    @sp.read_timeout = 500
    @ready = true
    @fill_in = false
    @position = []
    @time = []
    @direction = []
    @msg = ''
    @tmp_msg = nil
    @finish = false
  end

  def send_char
    @sp.write('a')
  end

  def fill_data
    if @fill_in 
      fill_position
      fill_direction
      fill_time
    end

  end

  #def send_setpoint(point)
   # @sp.write(point)
  #end

  def handle_msg
    case @msg
    when "Start" then @fill_in, @finish = true, false 
    when "Koniec" then @fill_in, @finish = false, true; finish
    else fill_data 
    end    
  end

  def handle_input
    if @tmp_msg
      if @tmp_msg != "#"
        @msg.concat(@tmp_msg)       
      else
        handle_msg
        @msg.clear
      end
    end
  end

  def fill_position
    data = @msg.split(":")[1]    
    @position << data
  end

  def fill_direction
    data = @msg.split(":")[2]
    @direction << data
  end

  def fill_time
    data = @msg.split(":")[3]
    @time << data
  end

  def finish
    if @finish
      @ready = false
      @finish = false
    end
  end

  def read_port
    while @ready do 
      @tmp_msg = @sp.getc
      handle_input
    end
    puts "Koniec"
  end
end
