class AddForeginKeys < ActiveRecord::Migration
  def change
    add_foreign_key :directions, :stats
    add_foreign_key :spent_times, :stats
    add_foreign_key :positions, :stats
  end
end
