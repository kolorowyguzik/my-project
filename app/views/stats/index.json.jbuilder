json.array!(@stats) do |stat|
  json.extract! stat, :id, :position, :time
  json.url stat_url(stat, format: :json)
end
