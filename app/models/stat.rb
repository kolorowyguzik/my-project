class Stat < ActiveRecord::Base
  has_many :spent_times, :dependent => :destroy 
  has_many :directions, :dependent => :destroy
  has_many :positions, :dependent => :destroy
end
