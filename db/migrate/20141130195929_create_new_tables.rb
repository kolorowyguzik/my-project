class CreateNewTables < ActiveRecord::Migration
  def change
    create_table :spent_times do |t|
      t.integer :stat_id
      t.float :data, precision: 8, scale: 4
      t.timestamps
    end
    create_table :positions do |t|
      t.integer :stat_id
      t.float :data, precision: 8, scale: 4
      t.timestamps
    end
    create_table :directions do |t|
      t.integer :stat_id
      t.float :data, precision: 8, scale: 4
      t.timestamps
    end
  end
end
