class StatsController < ApplicationController
  before_action :set_stat, only: [:show, :edit, :update, :destroy]

  # GET /stats
  # GET /stats.json
  def index
    @stats = Stat.all
  end


 # def setpoint
  #  setpoint = params[:setpoint]

   # handler = port_handler
    #handler.send_setpoint(setpoint)
    #redirect_to root_path, notice: 'Setpoint successfully sent!'

 # end
  # GET /stats/1
  # GET /stats/1.json
  def show
   # @times = @stat.spent_times.pluck(:data)
    @position = @stat.positions.pluck(:data)
    @chart = LazyHighCharts::HighChart.new('basic_line') do |f|
   f.chart( {
            type: 'spline'
        })
   f.title( {
            text: 'Stats'
        })
          f.xAxis({
            title:{
             text: 'Time'
            }
        })
        f.yAxis( {
            title: {
                text: 'Position'
            }

        })

        f.plotOptions( {
            spline: {
                lineWidth: 1,
                states: {
                    hover: {
                        lineWidth: 2
                    }
                },
                marker: {
                    enabled: false
                }


            }
        })
        f.series({
            name: 'Position',
            data: @position

        } )
  end

  end

  # GET /stats/new
  def new
    @stat = Stat.new
  end

  # GET /stats/1/edit
  def edit
  end

  # POST /stats
  # POST /stats.json
  def create
    @stat = Stat.new(stat_params)

    respond_to do |format|
      if @stat.save
        format.html { redirect_to @stat, notice: 'Stat was successfully created.' }
        format.json { render :show, status: :created, location: @stat }
      else
        format.html { render :new }
        format.json { render json: @stat.errors, status: :unprocessable_entity }
      end
    end
  end

  def start_handler

    stat = Stat.new
    handler = port_handler
    handler.send_char
    handler.read_port

    @position = handler.position.map(&:to_i)
    @time = handler.time.map(&:to_i)
    @direction = handler.direction.map(&:to_i)

    @direction.each do |x|
      Direction.create(stat: stat, data: x)
    end

    @time.each do |x|
      SpentTime.create(stat: stat, data: x)
    end

    @position.each do |x|
      Position.create(stat: stat, data: x)
    end

    @chart = LazyHighCharts::HighChart.new('basic_line') do |f|
     f.chart( {
            type: 'spline'
        })
   f.title( {
            text: 'Stats'
        })
          f.xAxis({
            title:{
             text: 'Time'
            }
        })
        f.yAxis( {
            title: {
                text: 'Position'
            }

        })

        f.plotOptions( {
            spline: {
                lineWidth: 1,
                states: {
                    hover: {
                        lineWidth: 2
                    }
                },
                marker: {
                    enabled: false
                }


            }
        })
        f.series({
            name: 'Position',
            data: @position

        } )
    end
    render :print_stats
  end

  def print_stats
  end

  # PATCH/PUT /stats/1
  # PATCH/PUT /stats/1.json
  def update
    respond_to do |format|
      if @stat.update(stat_params)
        format.html { redirect_to @stat, notice: 'Stat was successfully updated.' }
        format.json { render :show, status: :ok, location: @stat }
      else
        format.html { render :edit }
        format.json { render json: @stat.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stats/1
  # DELETE /stats/1.json
  def destroy
    @stat.destroy
    respond_to do |format|
      format.html { redirect_to stats_url, notice: 'Stat was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_stat
    @stat = Stat.find(params[:id])
  end

  def port_handler
    @port_handler ||= PortHandler.new
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def stat_params
    params.require(:stat).permit(:position, :time)
  end
end
