# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150128130855) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "directions", force: true do |t|
    t.integer  "stat_id"
    t.float    "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "positions", force: true do |t|
    t.integer  "stat_id"
    t.float    "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "spent_times", force: true do |t|
    t.integer  "stat_id"
    t.float    "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "stats", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_foreign_key "directions", "stats", name: "directions_stat_id_fk"

  add_foreign_key "positions", "stats", name: "positions_stat_id_fk"

  add_foreign_key "spent_times", "stats", name: "spent_times_stat_id_fk"

end
